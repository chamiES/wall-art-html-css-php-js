<!--A Design by W3layouts 
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
<title>The Wall Art - The art gallary - Photographs</title>
<link href="css/bootstrap-3.1.1.min.css" rel='stylesheet' type='text/css' />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.min.js"></script>
<!-- Custom Theme files -->
<!--theme-style-->
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />	
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Youth Fashion Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href='//fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Poiret+One' rel='stylesheet' type='text/css'>
<!-- start menu -->
<script src="js/bootstrap.min.js"></script>
<script src="js/simpleCart.min.js"> </script>
<!-- slide -->
<script src="js/responsiveslides.min.js"></script>
   <script>
    $(function () {
      $("#slider").responsiveSlides({
      	auto: true,
      	speed: 500,
        namespace: "callbacks",
        pager: true,
      });
    });
  </script>
  <style>
        #button{
			background-color:black;
			color:#9484E2;
			width:150px;
			height:50px;
			
			}
			#button:hover{
			 	background-color:#E116D4;
				
				}
			#button1{
			background-color:black;
			color:#9484E2;
			width:150px;
			height:50px;
			
			
			}
			#button1:hover{
			 	background-color:#E116D4;
				}
        </style>
</head>
<body>
<!--header-->
<div class="header">
	<div class="header-top">
		<div class="container">
				<div class="col-sm-4 logo">
					<h1><a href="../index.php">The Wall <span>Art</span></a></h1>	
				</div>
			<div class="col-sm-4 world">
					<div class="cart box_1">
						<a href="checkout.html">
						
							
					</div>
			</div>
			<div class="col-sm-2 number">
					<span><i class="glyphicon glyphicon-phone"></i>071 5 567 567</span>
					<p>Call me</p>
				</div>
			<div class="col-sm-2 search">		
				<a class="play-icon popup-with-zoom-anim" href="#small-dialog"><i class="glyphicon glyphicon-search"> </i> </a>
			</div>
				<div class="clearfix"> </div>
		</div>
	</div>
		<div class="container">
			<div class="head-top">
			<div class="n-avigation">
			
				<nav class="navbar nav_bottom" role="navigation">
				
				 <!-- Brand and toggle get grouped for better mobile display -->
				  <div class="navbar-header nav_2">
					  <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					  </button>
					  <a class="navbar-brand" href="#"></a>
				   </div> 
				   <!-- Collect the nav links, forms, and other content for toggling -->
					 <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
                                <ul class="nav navbar-nav nav_1">
                                    <li><a href="../index.php">Home</a></li>
                                    <li class="dropdown mega-dropdown active">
                                        <a href="paintings.php" class="dropdown-toggle" data-toggle="dropdown">Paintings<span class="caret"></span></a>				

                                        <div class="dropdown-menu mega-dropdown-menu">
                                            <div class="container-fluid">
                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="men">
                                                        <ul class="nav-list list-inline">
                                                            <li><a href="item2.php"><img src="images/t7.jpg" class="img-responsive" alt=""/></a></li>
                                                            <li><a href="item1.php"><img src="images/t8.jpg" class="img-responsive" alt=""/></a></li>
                                                            <li><a href="item5.php"><img src="images/t9.jpg" class="img-responsive" alt=""/></a></li>
                                                            <li><a href="item9.php"><img src="images/t11.jpg" class="img-responsive" alt=""/></a></li>
                                                            <li><a href="item4.php"><img src="images/t1.jpg" class="img-responsive" alt=""/></a></li>
                                                            <li><a href="../paintings.php"><img src="images/t12.jpg" class="img-responsive" alt=""/></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Nav tabs -->

                                        </div>				
                                    </li>
                                    <li class="dropdown mega-dropdown active">
                                        <a href="PhotographsSign.php" class="dropdown-toggle" data-toggle="dropdown">Photographs<span class="caret"></span></a>				
                                        <div class="dropdown-menu mega-dropdown-menu">
                                            <div class="container-fluid">
                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="men">
                                                        <ul class="nav-list list-inline">
                                                            <li><a href="item12.php"><img src="images/t10.jpg" class="img-responsive" alt=""/></a></li>
                                                            <li><a href="item11.php"><img src="images/t2.jpg" class="img-responsive" alt=""/></a></li>
                                                            <li><a href="item3.php"><img src="images/t3.jpg" class="img-responsive" alt=""/></a></li>
                                                            <li><a href="item19.php"><img src="images/t4.jpg" class="img-responsive" alt=""/></a></li>
                                                            <li><a href="item20.php"><img src="images/t5.jpg" class="img-responsive" alt=""/></a></li>
                                                            <li><a href="../photographs.php"><img src="images/t6.jpg" class="img-responsive" alt=""/></a></li>
                                                        </ul>
                                                    </div>

                                                </div>
                                            </div>
                                            <!-- Nav tabs -->

                                        </div>				
                                    </li>
                                    <li class="dropdown mega-dropdown active">
                                        <a href="ArtSign.php" class="dropdown-toggle" data-toggle="dropdown">Art & Crafts<span class="caret"></span></a>				
                                        <div class="dropdown-menu mega-dropdown-menu">
                                            <div class="container-fluid">
                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="men">
                                                        <ul class="nav-list list-inline">
                                                            <li><a href="item15.php"><img src="images/a1.jpg" class="img-responsive" alt=""/></a></li>
                                                            <li><a href="item16.php"><img src="images/a2.jpg" class="img-responsive" alt=""/></a></li>
                                                            <li><a href="item17.php"><img src="images/a3.jpg" class="img-responsive" alt=""/></a></li>
                                                            <li><a href="item21.php"><img src="images/a4.jpg" class="img-responsive" alt=""/></a></li>
                                                            <li><a href="item22.php"><img src="images/a5.jpg" class="img-responsive" alt=""/></a></li>
                                                            <li><a href="../Art.php"><img src="images/t6.jpg" class="img-responsive" alt=""/></a></li>
                                                        </ul>
                                                    </div>

                                                </div>
                                            </div>
                                            <!-- Nav tabs -->

                                        </div>				
                                    </li>
                                    <li><a href="../products.php">Products</a></li>
									<li><a href="../account.php">Sign In</a></li>
                                    <li class="last"><a href="../contact.php">Contact</a></li>
                            
                            
                            
						</ul>
					 </div><!-- /.navbar-collapse -->
				  
				</nav>
			</div>

				
		<div class="clearfix"> </div>
			<!---pop-up-box---->   
					<link href="css/popuo-box.css" rel="stylesheet" type="text/css" media="all"/>
					<script src="js/jquery.magnific-popup.js" type="text/javascript"></script>
					<!---//pop-up-box---->
				<div id="small-dialog" class="mfp-hide">
				<div class="search-top">
						<div class="login">
							<form action="#" method="post">
								<input type="submit" value="">
								<input type="text" name="search" value="Type something..." onfocus="this.value = '';" onblur="if (this.value == '') {this.value = '';}">		
							
							</form>
						</div>
						<p>	Shopping</p>
					</div>				
				</div>
				 <script>
						$(document).ready(function() {
						$('.popup-with-zoom-anim').magnificPopup({
							type: 'inline',
							fixedContentPos: false,
							fixedBgPos: true,
							overflowY: 'auto',
							closeBtnInside: true,
							preloader: false,
							midClick: true,
							removalDelay: 300,
							mainClass: 'my-mfp-zoom-in'
						});
																						
						});
				</script>			
	<!---->		
		</div>
	</div>
</div>
<div class="breadcrumbs">
		<div class="container">
			<ol class="breadcrumb breadcrumb1 animated wow slideInLeft animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: slideInLeft;">
				<li><a href="index.html"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a></li>
				<li class="active">Item 11</li>
			</ol>
		</div>
	</div>
<div class="single">

<div class="container">
<div class="col-md-9">
	<div class="col-md-5 grid">		
		<div class="flexslider">
			  <ul class="slides">
			    <li data-thumb="images/si.jpg">
                                <div class="thumb-image"> <img src="images/item/i11.png" data-imagezoom="true" class="img-responsive"> </div>
			    </li>
			    
			  </ul>
		</div>
	</div>	
<div class="col-md-7 single-top-in">
						<div class="single-para simpleCart_shelfItem">
							<h2>UP</h2>
							<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.</p>
														
								<label  class="add-to item_price">Rs 4000</label>
							
							
								
						</div>
                       <br><a href="../account.php" ><input type="submit" id="button" value="Add to Cart" /></a><br>
                          
            <form method="post">
						
						
                        <input type="submit" id="button1" value="HIRE THE SELLER" name="hire"/>
                       
					</form>		
					</div>
			<div class="clearfix"> </div>
			<div class="content-top1">
				<div class="col-md-4 col-md4">
					<div class="col-md1 simpleCart_shelfItem">
						<a href="single.html">
							<a href="item5.php"><img class="img-responsive" src="images/pi6.png" alt="" /></a>
						</a>
						<h3><a href="item5.php">Item 5</a></h3>
						<div class="price">
								<h5 class="item_price">Rs 3500</h5>
								
								<div class="clearfix"> </div>
						</div>
					</div>
				</div>	
			<div class="col-md-4 col-md4">
					<div class="col-md1 simpleCart_shelfItem">
						<a href="single.html">
							<a href="item13.php"><img class="img-responsive" src="images/pi7.png" alt="" /></a>
						</a>
						<h3><a href="item13.php">Item 13</a></h3>
						<div class="price">
								<h5 class="item_price">Rs 3000</h5>
								
								<div class="clearfix"> </div>
						</div>
						
					</div>
				</div>	
			<div class="col-md-4 col-md4">
					<div class="col-md1 simpleCart_shelfItem">
						<a href="single.html">
							<a href="item12.php"><img class="img-responsive" src="images/pi.png" alt="" /></a>
						</a>
						<h3><a href="item12.php">Item 12</a></h3>
						<div class="price">
								<h5 class="item_price">Rs 3500</h5>
								
								<div class="clearfix"> </div>
						</div>
						
					</div>
				</div>	
			
			<div class="clearfix"> </div>
			</div>		
</div>
<!----->
<div class="col-md-3 product-bottom">
			<!--categories-->
				<div class=" rsidebar span_1_of_left">
						<h3 class="cate">Categories</h3>
							 <ul class="menu-drop">
							<li class="item1"><a href="#">Phographs </a>
								<ul class="cute">
									<li class="subitem1"><a href="single.html">Landscapes </a></li>
									<li class="subitem2"><a href="single.html">Animals </a></li>
									<li class="subitem3"><a href="single.html">Nature </a></li>
                                    <li class="subitem3"><a href="single.html">Other </a></li>
								</ul>
							</li>
							<li class="item2"><a href="#">Paintings </a>
								<ul class="cute">
									<li class="subitem1"><a href="single.html">Pencil Drawings </a></li>
									<li class="subitem2"><a href="single.html">Water Color </a></li>
									<li class="subitem3"><a href="single.html">Oil Paintings </a></li>
                                    <li class="subitem3"><a href="single.html">Other </a></li>
								</ul>
							</li>
							<li class="item3"><a href="#">Art and Crafts</a>
								<ul class="cute">
									<li class="subitem1"><a href="single.html">wooden </a></li>
									<li class="subitem2"><a href="single.html">wall decorations </a></li>
									<li class="subitem3"><a href="single.html">flower decorations</a></li>
                                    <li class="subitem3"><a href="single.html">Other</a></li>
								</ul>
							</li>
							<li class="item4"><a href="#">Other</a>
								<ul class="cute">
									<li class="subitem1"><a href="single.html">Other </a></li>
									
								</ul>
							</li>
									
							
				<!--initiate accordion-->
						<script type="text/javascript">
							$(function() {
							    var menu_ul = $('.menu-drop > li > ul'),
							           menu_a  = $('.menu-drop > li > a');
							    menu_ul.hide();
							    menu_a.click(function(e) {
							        e.preventDefault();
							        if(!$(this).hasClass('active')) {
							            menu_a.removeClass('active');
							            menu_ul.filter(':visible').slideUp('normal');
							            $(this).addClass('active').next().stop(true,true).slideDown('normal');
							        } else {
							            $(this).removeClass('active');
							            $(this).next().stop(true,true).slideUp('normal');
							        }
							    });
							
							});
						</script>
<!--//menu-->
<!--seller-->
				<div class="product-bottom">
						<h3 class="cate">Top Selling</h3>
					<div class="product-go">
						<div class=" fashion-grid">
							<a href="item10.php"><img class="img-responsive " src="images/pr.jpg" alt=""></a>	
						</div>
						<div class=" fashion-grid1">
							<h6 class="best2"><a href="single.html" >  </a></h6>
							<span class=" price-in1"> Rs 4000</span>
						</div>	
						<div class="clearfix"> </div>
					</div>
					<div class="product-go">
						<div class=" fashion-grid">
							<a href="item6.php"><img class="img-responsive " src="images/pr1.jpg" alt=""></a>	
						</div>
						<div class=" fashion-grid1">
							<h6 class="best2"><a href="single.html" >
                             </a></h6>
							<span class=" price-in1"> Rs 3500</span>
						</div>	
						<div class="clearfix"> </div>
					</div>
					<div class="product-go">
						<div class=" fashion-grid">
							<a href="item14.php"><img class="img-responsive " src="images/pr2.jpg" alt=""></a>	
						</div>
						<div class=" fashion-grid1">
							<h6 class="best2"><a href="single.html" > </a></h6>
							<span class=" price-in1"> Rs 4000</span>
						</div>	
						<div class="clearfix"> </div>
					</div>	
					<div class="product-go">
						<div class=" fashion-grid">
							<a href="item9.php"><img class="img-responsive " src="images/pr3.jpg" alt=""></a>	
						</div>
					  <div class=" fashion-grid1">
							<h6 class="best2"><a href="single.html" >  </a></h6>
							<span class=" price-in1"> Rs 3500</span>
						</div>	
						<div class="clearfix"> </div>
					</div>		
				</div>

<!--//seller-->
<!--tag-->
				<div class="tag">	
						
		</div>
		<div class="clearfix"> </div>
	</div>
</div>
		
<!--footer-->
<div class="footer">
	<div class="container">
		<div class="footer-top">
			<div class="col-md-6 top-footer">
				<h3>Follow Us On</h3>
				<div class="social-icons">
					<ul class="social">
						<li><a href="#"><i></i></a> </li>
						<li><a href="#"><i class="facebook"></i></a></li>	
						<li><a href="#"><i class="google"></i> </a></li>
						<li><a href="#"><i class="linked"></i> </a></li>						
					</ul>
						<div class="clearfix"></div>
				 </div>
			</div>
			<div class="col-md-6 top-footer1">
				<h3>Newsletter</h3>
					<form action="#" method="post">
						<input type="text" name="email" value="" onfocus="this.value='';" onblur="if (this.value == '') {this.value ='';}">
						<input type="submit" value="SUBSCRIBE">
					</form>
			</div>
			<div class="clearfix"> </div>	
		</div>	
	</div>
		<div class="footer-bottom">
		<div class="container">
				<div class="col-md-3 footer-bottom-cate animated wow fadeInLeft" data-wow-delay=".5s">
					<h6>Categories</h6>
					<ul>
						<li><a href="../products.php">All Categories</a></li>
						<li><a href="../paintings.php">Paintings</a></li>
						<li><a href="../photographs.php">Photographs</a></li>
						<li><a href="../Art.php">Art & Crafts</a></li>
						
						
					</ul>
				</div>
				<div class="col-md-3 footer-bottom-cate animated wow fadeInLeft" data-wow-delay=".5s">
					<h6>Feature Works</h6>
					<ul>
						<li><a href="item1.php">WORK 1</a></li>
						<li><a href="item2.php">WORK 2</a></li>
						<li><a href="item3.php">WORK 3</a></li>
						<li><a href="item4.php">WORK 4</a></li>
						<li><a href="item5.php">WORK 5</a></li>
					</ul>
				</div>
				<div class="col-md-3 footer-bottom-cate animated wow fadeInRight" data-wow-delay=".5s">
					<h6>Top Works</h6>
					<ul>
						<li><a href="item7.php">Top Work 1</a></li>
						<li><a href="item8.php">Top Work 2</a></li>
						<li><a href="item9.php">Top Work 3</a></li>
						<li><a href="item10.php">Top Work 4</a></li>
						<li><a href="item11.php">Top Work 5</a></li>
						
						
					</ul>
				</div>
				<div class="col-md-3 footer-bottom-cate cate-bottom animated wow fadeInRight" data-wow-delay=".5s">
					<h6>Contact Us</h6>
					<ul>
						<li><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i>Address : Wayamba University</li>
						<li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i>Email : <a href="mailto:info@example.com">info@example.com</a></li>
						<li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>Phone : +94 71 5 567 567</li>
					</ul>
				</div>
				<div class="clearfix"> </div>
				<p class="footer-class animated wow fadeInUp animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInUp;"> © 2017 THE WALL ART  - The Art Gallery . All Rights Reserved | Design by WUSL </p>
			</div>
	</div>
</div>
        <!--footer-->
<!--footer-->
<!-- slide -->
<script src="js/jquery.min.js"></script>
<script src="js/imagezoom.js"></script>
<!-- start menu -->
<script type="text/javascript" src="js/memenu.js"></script>
<script>$(document).ready(function(){$(".memenu").memenu();});</script>
<script src="js/simpleCart.min.js"> </script>
<!--initiate accordion-->
						<script type="text/javascript">
							$(function() {
							    var menu_ul = $('.menu-drop > li > ul'),
							           menu_a  = $('.menu-drop > li > a');
							    menu_ul.hide();
							    menu_a.click(function(e) {
							        e.preventDefault();
							        if(!$(this).hasClass('active')) {
							            menu_a.removeClass('active');
							            menu_ul.filter(':visible').slideUp('normal');
							            $(this).addClass('active').next().stop(true,true).slideDown('normal');
							        } else {
							            $(this).removeClass('active');
							            $(this).next().stop(true,true).slideUp('normal');
							        }
							    });
							
							});
						</script>
						<!-- FlexSlider -->
  <script defer src="js/jquery.flexslider.js"></script>
<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />

<script>
// Can also be used with $(document).ready()
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide",
    controlNav: "thumbnails"
  });
});
</script>
<!---pop-up-box---->
					<link href="css/popuo-box.css" rel="stylesheet" type="text/css" media="all"/>
					<script src="js/jquery.magnific-popup.js" type="text/javascript"></script>
					<!---//pop-up-box---->
					 <script>
						$(document).ready(function() {
						$('.popup-with-zoom-anim').magnificPopup({
							type: 'inline',
							fixedContentPos: false,
							fixedBgPos: true,
							overflowY: 'auto',
							closeBtnInside: true,
							preloader: false,
							midClick: true,
							removalDelay: 300,
							mainClass: 'my-mfp-zoom-in'
						});
																						
						});
				</script>	
</body>
</html>