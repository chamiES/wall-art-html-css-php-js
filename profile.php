<?php
session_start();
if (isset($_POST['submit'])) {
    $con = mysqli_connect("localhost", "root", "", "wallartDB");
    $fname = $_POST['fname'];
    $lname = $_POST['lname'];
    $email = $_POST['email'];
    $shp1 = $_POST['shippingAddress1'];
    $shp2 = $_POST['shippingAddress2'];
    $shp3 = $_POST['shippingAddress3'];
    $shp4 = $_POST['shippingAddress4'];
    $contact = $_POST['contact'];    
    $emailValidation = "/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9]+(\.[a-z]{2,3})$/";
    $number = "/^[0-9]+$/";
    $up = $_SESSION['fname'];
    $sql = "UPDATE register SET register.fname = '$fname', register.lname= '$lname',register.email='$email',register.address1='$shp1',register.address2='$shp2', register.address3 ='$shp3',register.address4 ='$shp4',register.ContactNo ='$contact' WHERE register.fname = '$up'";
    
   if (!preg_match($emailValidation, $email)) {
        echo'<script language="javascript">
        	window.alert("Invalid email format..!");
                window.location.href="profile.php";
         	</script>';
    } elseif (!preg_match($number, $contact)) {
        echo'<script language="javascript">
        	window.alert("Invalid contact number..!. use only numbers");
                window.location.href="profile.php";
         	</script>';
    } else {
        mysqli_query($con, $sql);
        echo'<script language="javascript">
        	window.alert("Save Changes Successfully.");
                window.location.href="profile.php";
         	</script>';
    }
}
?>  
<!DOCTYPE html>
<html>
    <head>
        <title>The Wall Art - The art gallary - Photographs</title>
        <link href="css/bootstrap-3.1.1.min.css" rel='stylesheet' type='text/css' />
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="js/jquery.min.js"></script>
        <!-- Custom Theme files -->
        <!--theme-style-->
        <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />	
        <!--//theme-style-->
        <meta name="viewport" content="width = device-width, initial-scale = 1">
        <meta http-equiv="Content-Type" content="text/html;
              charset = utf-8" />
        <meta name="keywords" content="Youth Fashion Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <link href='//fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Poiret+One' rel='stylesheet' type='text/css'>
        <!-- start menu -->
        <script src="js/bootstrap.min.js"></script>
        <script src="js/simpleCart.min.js"></script>
        <!-- slide -->
        <script src="js/responsiveslides.min.js"></script>
        <script>
            $(function () {
                $("#slider").responsiveSlides({
                    auto: true,
                    speed: 500,
                    namespace: "callbacks",
                    pager: true,
                });
            });
        </script>
    </head>
    <body>
        <!--header-->
        <div class="header">
            <div class="header-top">
                <div class="container">
                    <div class="col-sm-4 logo">
                        <h1><a href="indexSign.php">The Wall <span>Art</span></a></h1>	
                    </div>
                    <div class="col-sm-4 world">
                        <div class="cart box_1">
                            <a href="checkout.php">
                                <h3> <div class="total">

                                    <img src="images/cart.png" alt=""/></h3>
                            </a>
                            <p><a href="javascript:;" class="simpleCart_empty">My Cart</a></p>

                        </div>
                    </div>
                    <div class="col-sm-2 number">
                        <span><i class="glyphicon glyphicon-phone"></i>071 5 567 567</span>
                        <p>Call me</p>
                    </div>
                    <div class="col-sm-2 search">		
                        <a class="play-icon popup-with-zoom-anim" href="#small-dialog"><i class="glyphicon glyphicon-search"> </i> </a>
                    </div>
                    <div class="clearfix"> </div>
                </div>
            </div>
            <div class="container">
                <div class="head-top">
                    <div class="n-avigation">

                        <nav class="navbar nav_bottom" role="navigation">

                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header nav_2">
                                <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand" href="#"></a>
                            </div> 
                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
                                <ul class="nav navbar-nav nav_1">
                                    <li><a href="indexSign.php">Home</a></li>
                                    <li class="dropdown mega-dropdown active">
                                        <a href="paintingsSign.php" class="dropdown-toggle" data-toggle="dropdown">Paintings<span class="caret"></span></a>				

                                        <div class="dropdown-menu mega-dropdown-menu">
                                            <div class="container-fluid">
                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="men">
                                                        <ul class="nav-list list-inline">
                                                            <li><a href="women.html"><img src="images/t7.jpg" class="img-responsive" alt=""/></a></li>
                                                            <li><a href="#"><img src="images/t8.jpg" class="img-responsive" alt=""/></a></li>
                                                            <li><a href="#"><img src="images/t9.jpg" class="img-responsive" alt=""/></a></li>
                                                            <li><a href="#"><img src="images/t11.jpg" class="img-responsive" alt=""/></a></li>
                                                            <li><a href="#"><img src="images/t1.jpg" class="img-responsive" alt=""/></a></li>
                                                            <li><a href="paintingsSign.php"><img src="images/t12.jpg" class="img-responsive" alt=""/></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Nav tabs -->

                                        </div>				
                                    </li>
                                    <li class="dropdown mega-dropdown active">
                                        <a href="PhotographsSign.php" class="dropdown-toggle" data-toggle="dropdown">Photographs<span class="caret"></span></a>				
                                        <div class="dropdown-menu mega-dropdown-menu">
                                            <div class="container-fluid">
                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="men">
                                                        <ul class="nav-list list-inline">
                                                            <li><a href="men.html"><img src="images/t10.jpg" class="img-responsive" alt=""/></a></li>
                                                            <li><a href="#"><img src="images/t2.jpg" class="img-responsive" alt=""/></a></li>
                                                            <li><a href="#"><img src="images/t3.jpg" class="img-responsive" alt=""/></a></li>
                                                            <li><a href="#"><img src="images/t4.jpg" class="img-responsive" alt=""/></a></li>
                                                            <li><a href="#"><img src="images/t5.jpg" class="img-responsive" alt=""/></a></li>
                                                            <li><a href="photographsSign.php"><img src="images/t6.jpg" class="img-responsive" alt=""/></a></li>
                                                        </ul>
                                                    </div>

                                                </div>
                                            </div>
                                            <!-- Nav tabs -->

                                        </div>				
                                    </li>
                                    <li class="dropdown mega-dropdown active">
                                        <a href="ArtSign.php" class="dropdown-toggle" data-toggle="dropdown">Art & Crafts<span class="caret"></span></a>				
                                        <div class="dropdown-menu mega-dropdown-menu">
                                            <div class="container-fluid">
                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="men">
                                                        <ul class="nav-list list-inline">
                                                            <li><a href="m en.html"><img src="images/a1.jpg" class="img-responsive" alt=""/></a></li>
                                                            <li><a href=""><img src="images/a2.jpg" class="img-responsive" alt=""/></a></li>
                                                            <li><a href=""><img src="images/a3.jpg" class="img-responsive" alt=""/></a></li>
                                                            <li><a href=""><img src="images/a4.jpg" class="img-responsive" alt=""/></a></li>
                                                            <li><a href=""><img src="images/a5.jpg" class="img-responsive" alt=""/></a></li>
                                                            <li><a href="ArtSign.php"><img src="images/t6.jpg" class="img-responsive" alt=""/></a></li>
                                                        </ul>
                                                    </div>

                                                </div>
                                            </div>
                                            <!-- Nav tabs -->

                                        </div>				
                                    </li>
                                    <li><a href="productsSign.php">Products</a></li>

                                    <li class="last"><a href="contactSign.php">Contact</a></li>

                                    <li class="dropdown mega-dropdown active">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php
                                            echo "Hi " . $_SESSION['fname'] . "..!";
                                            ?><span class="caret"></span></a>				
                                        <div class="dropdown-menu mega-dropdown-menu">
                                            <div class="container-fluid">
                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="men">
                                                        <ul class="nav-list list-inline">
                                                            <li><a href="profile.php"><img src="images/p2.jpg" class="img-responsive" alt=""/></a></li>
                                                            <li><a href="MyBuying.php"><img src="images/p3.jpg" class="img-responsive" alt=""/></a></li>
                                                            <li><a href="MySelling.php"><img src="images/p4.jpg" class="img-responsive" alt=""/></a></li>
                                                             <li><a href="MyMessages.php"><img src="images/p5.jpg" class="img-responsive" alt=""/></a></li> 
                                                             <li><a href="MyMessagesSent.php"><img src="images/p6.jpg" class="img-responsive" alt=""/></a></li> 
                                                             <li><a href="index.php"><img src="images/p1.jpg" class="img-responsive" alt=""/></a></li>
                                                        </ul>
                                                    </div>

                                                </div>
                                            </div>
                                            <!-- Nav tabs -->

                                        </div>				
                                    </li>

                                </ul>
                            </div><!-- /.navbar-collapse -->

                        </nav>
                    </div>

                    <div class="clearfix"> </div>
                    <!---pop-up-box---->   
                    <link href="css/popuo-box.css" rel="stylesheet" type="text/css" media="all"/>
                    <script src="js/jquery.magnific-popup.js" type="text/javascript"></script>
                    <!---//pop-up-box---->
                    <div id="small-dialog" class="mfp-hide">
                        <div class="search-top">
                            <div class="login">
                                <form action="#" method="post">
                                    <input type="submit" value="">
                                    <input type="text" name="search" value="Type something..." onfocus="this.value = '';" onblur="if (this.value == '') {
                                                this.value = '';
                                            }">		

                                </form>
                            </div>
                            <p>	Shopping</p>
                        </div>				
                    </div>
                    <script>
                        $(document).ready(function () {
                            $('.popup-with-zoom-anim').magnificPopup({
                                type: 'inline',
                                fixedContentPos: false,
                                fixedBgPos: true,
                                overflowY: 'auto',
                                closeBtnInside: true,
                                preloader: false,
                                midClick: true,
                                removalDelay: 300,
                                mainClass: 'my-mfp-zoom-in'
                            });

                        });
                    </script>			
                    <!---->		
                </div>
            </div>
        </div>
        <!--//header-->
        <div class="breadcrumbs">
            <div class="container">
                <ol class="breadcrumb breadcrumb1 animated wow slideInLeft animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: slideInLeft;">
                    <li><a href="indexSign.php"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a></li>
                    <li class="active">Register</li>
                </ol>
            </div>
        </div>
        <div class="container">
            <div class="register">
                <h2>Profile</h2>
                <form method="post">
                    <div class="col-md-6  register-top-grid">
                        <?php
                        $con = mysqli_connect("localhost", "root", "", "wallartDB");
                        $user = $_SESSION['fname'];

                        $sql = "SELECT * FROM register WHERE register.fname = '$user'";
                        $result = mysqli_query($con, $sql);

                        while ($row = mysqli_fetch_assoc($result)) {
                            $fname1 = $row['fname'];
                            $lname1 = $row['lname'];
                            $email1 = $row['email'];
                            $shp11 = $row['address1'];
                            $shp21 = $row['address2'];
                            $shp31 = $row['address3'];
                            $shp41 = $row['address4'];
                            $contact1 = $row['ContactNo'];
                            $password1 = $row['pasword'];
                        }
                        ?>
                        <div class="mation" >
                            <span><font color="#DF0FD1"> First Name</font></span>
                            <input type="text" name="fname" value="<?php echo $fname1; ?>" required > 

                            <span><font color="#DF0FD1">Last Name</font></span>
                            <input type="text" name="lname" required value="<?php echo $lname1; ?>"> 

                            <span><font color="#DF0FD1">Email Address</font></span>
                            <input type="text" name="email" required value="<?php echo $email1; ?>">

                            <span><font color="#DF0FD1">Shipping Address</font></span>
                            <input type="text" name="shippingAddress1" placeholder="Address line 1" required value="<?php echo $shp11; ?>">
                            <input type="text" name="shippingAddress2" placeholder="Address line 2" required value="<?php echo $shp21; ?>">
                            <input type="text" name="shippingAddress3" placeholder="Address line 3(Optional)" value="<?php echo $shp31; ?>">
                            <input type="text" name="shippingAddress4" placeholder="Address line 4(Optional)" value="<?php echo $shp41; ?>">

                            <span><font color="#DF0FD1">Contact Number</font></span>
                            <input type="text" name="contact" required value="<?php echo $contact1; ?>"> <br><br>




                            <input type="submit" value="Save Changes" name="submit" ></span>

                        </div>
                        <div class="clearfix"> </div>

                    </div>
                    <div class=" col-md-6 register-bottom-grid">

                        <div class="mation">
                            <img src="images/11.jpg">


                        </div>
                    </div>
                    <div class="clearfix"> </div>
                </form>

                <div class="register-but">
                    <form method="post">

                        <div class="clearfix"> </div>
                    </form>
                </div>
            </div>
        </div>

        <!--footer-->
        <div class="footer">
            <div class="container">
                <div class="footer-top">
                    <div class="col-md-6 top-footer">
                        <h3>Follow Us On</h3>
                        <div class="social-icons">
                            <ul class="social">
                                <li><a href="#"><i></i></a> </li>
                                <li><a href="#"><i class="facebook"></i></a></li>	
                                <li><a href="#"><i class="google"></i> </a></li>
                                <li><a href="#"><i class="linked"></i> </a></li>						
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="col-md-6 top-footer1">
                        <h3>Newsletter</h3>
                        <form action="#" method="post">
                            <input type="text" name="email" value="" onfocus="this.value = '';" onblur="if (this.value == '') {
                                        this.value = '';
                                    }">
                            <input type="submit" value="SUBSCRIBE">
                        </form>
                    </div>
                    <div class="clearfix"> </div>	
                </div>	
            </div>
           <div class="footer-bottom">
		<div class="container">
				<div class="col-md-3 footer-bottom-cate animated wow fadeInLeft" data-wow-delay=".5s">
					<h6>Categories</h6>
					<ul>
						<li><a href="productsSign.php">All Categories</a></li>
						<li><a href="PaintingsSign.php">Paintings</a></li>
						<li><a href="PhotographsSign.php">Photographs</a></li>
						<li><a href="ArtSign.php">Art & Crafts</a></li>
						
						
					</ul>
				</div>
				<div class="col-md-3 footer-bottom-cate animated wow fadeInLeft" data-wow-delay=".5s">
					<h6>Feature Works</h6>
					<ul>
						<li><a href="item/item1.php">WORK 1</a></li>
						<li><a href="item/item2.php">WORK 2</a></li>
						<li><a href="item/item3.php">WORK 3</a></li>
						<li><a href="item/item4.php">WORK 4</a></li>
						<li><a href="item/item5.php">WORK 5</a></li>
					</ul>
				</div>
				<div class="col-md-3 footer-bottom-cate animated wow fadeInRight" data-wow-delay=".5s">
					<h6>Top Works</h6>
					<ul>
						<li><a href="item/item7.php">Top Work 1</a></li>
						<li><a href="item/item8.php">Top Work 2</a></li>
						<li><a href="item/item9.php">Top Work 3</a></li>
						<li><a href="item/item10.php">Top Work 4</a></li>
						<li><a href="item/item11.php">Top Work 5</a></li>
						
						
					</ul>
				</div>
				<div class="col-md-3 footer-bottom-cate cate-bottom animated wow fadeInRight" data-wow-delay=".5s">
					<h6>Contact Us</h6>
					<ul>
						<li><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i>Address : Wayamba University</li>
						<li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i>Email : <a href="mailto:info@example.com">info@example.com</a></li>
						<li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>Phone : +94 71 5 567 567</li>
					</ul>
				</div>
				<div class="clearfix"> </div>
				<p class="footer-class animated wow fadeInUp animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInUp;"> © 2017 THE WALL ART  - The Art Gallery . All Rights Reserved | Design by WUSL </p>
			</div>
	</div>
</div>
        <!--footer-->
    </body>
</html>