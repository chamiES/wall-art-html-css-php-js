<!DOCTYPE html>
<html>
<head>
<style>
.buttonnew{
	background-color:#404040;
	color:white;
	border:none;
	border-radius:2px;
	padding:10px 24px;
	}
	.buttonnew:hover{
		background-color:#D907CB;
		
		}
		
        h7{
			color:#DF0FD1;
			font-size:50px;
			
			}
        
</style>
<title>The Wall Art - The art gallary</title>
<link href="css/bootstrap-3.1.1.min.css" rel='stylesheet' type='text/css' />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.min.js"></script>
<!-- Custom Theme files -->
<!--theme-style-->
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />	
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Youth Fashion Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href='//fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Poiret+One' rel='stylesheet' type='text/css'>
<!-- start menu -->
<script src="js/bootstrap.min.js"></script>
<script src="js/simpleCart.min.js"> </script>
<!-- slide -->
<script src="js/responsiveslides.min.js"></script>
   <script>
    $(function () {
      $("#slider").responsiveSlides({
      	auto: false,
      	speed: 500,
        namespace: "callbacks",
        pager: true,
      });
    });
  </script>
  <!-- animation-effect -->
<link href="css/animate.min.css" rel="stylesheet"> 
<script src="js/wow.min.js"></script>
<script>
 new WOW().init();
</script>
<!-- //animation-effect -->
</head>
<body>
<!--header-->
<div class="header">
	<div class="header-top">
		<div class="container">
				<div class="col-sm-4 logo animated wow fadeInLeft" data-wow-delay=".5s">
					<h1><a href="index.php">The Wall <span>Art</span></a></h1>	
				</div>
			<div class="col-sm-4 world animated wow fadeInRight" data-wow-delay=".5s">
					<div class="cart box_1">
						<a href="checkout.php">
						
						

					</div>
			</div>
			<div class="col-sm-2 number animated wow fadeInRight" data-wow-delay=".5s">
					<span><i class="glyphicon glyphicon-phone"></i>071 5 567 567</span>
					<p>Call me</p>
				</div>
			<div class="col-sm-2 search animated wow fadeInRight" data-wow-delay=".5s">		
				<a class="play-icon popup-with-zoom-anim" href="#small-dialog"><i class="glyphicon glyphicon-search"> </i> </a>
			</div>
				<div class="clearfix"> </div>
                
		</div>
	</div>
    
		<div class="container">
			<div class="head-top">
			<div class="n-avigation">
			
				<nav class="navbar nav_bottom" role="navigation">
				
				 <!-- Brand and toggle get grouped for better mobile display -->
				  <div class="navbar-header nav_2">
					  <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					  </button>
					  <a class="navbar-brand" href="#"></a>
				   </div> 
				   <!-- Collect the nav links, forms, and other content for toggling -->
					 <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
                                <ul class="nav navbar-nav nav_1">
                                    <li><a href="index.php">Home</a></li>
                                    <li class="dropdown mega-dropdown active">
                                        <a href="paintings.php" class="dropdown-toggle" data-toggle="dropdown">Paintings<span class="caret"></span></a>				

                                        <div class="dropdown-menu mega-dropdown-menu">
                                            <div class="container-fluid">
                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="men">
                                                        <ul class="nav-list list-inline">
                                                            <li><a href="item/item2.php"><img src="images/t7.jpg" class="img-responsive" alt=""/></a></li>
                                                            <li><a href="item/item1.php"><img src="images/t8.jpg" class="img-responsive" alt=""/></a></li>
                                                            <li><a href="item/item5.php"><img src="images/t9.jpg" class="img-responsive" alt=""/></a></li>
                                                            <li><a href="item/item9.php"><img src="images/t11.jpg" class="img-responsive" alt=""/></a></li>
                                                            <li><a href="item/item4.php"><img src="images/t1.jpg" class="img-responsive" alt=""/></a></li>
                                                            <li><a href="paintings.php"><img src="images/t12.jpg" class="img-responsive" alt=""/></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Nav tabs -->

                                        </div>				
                                    </li>
                                    <li class="dropdown mega-dropdown active">
                                        <a href="PhotographsSign.php" class="dropdown-toggle" data-toggle="dropdown">Photographs<span class="caret"></span></a>				
                                        <div class="dropdown-menu mega-dropdown-menu">
                                            <div class="container-fluid">
                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="men">
                                                        <ul class="nav-list list-inline">
                                                            <li><a href="item/item12.php"><img src="images/t10.jpg" class="img-responsive" alt=""/></a></li>
                                                            <li><a href="item/item11.php"><img src="images/t2.jpg" class="img-responsive" alt=""/></a></li>
                                                            <li><a href="item/item3.php"><img src="images/t3.jpg" class="img-responsive" alt=""/></a></li>
                                                            <li><a href="item/item19.php"><img src="images/t4.jpg" class="img-responsive" alt=""/></a></li>
                                                            <li><a href="item/item20.php"><img src="images/t5.jpg" class="img-responsive" alt=""/></a></li>
                                                            <li><a href="photographs.php"><img src="images/t6.jpg" class="img-responsive" alt=""/></a></li>
                                                        </ul>
                                                    </div>

                                                </div>
                                            </div>
                                            <!-- Nav tabs -->

                                        </div>				
                                    </li>
                                    <li class="dropdown mega-dropdown active">
                                        <a href="ArtSign.php" class="dropdown-toggle" data-toggle="dropdown">Art & Crafts<span class="caret"></span></a>				
                                        <div class="dropdown-menu mega-dropdown-menu">
                                            <div class="container-fluid">
                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="men">
                                                        <ul class="nav-list list-inline">
                                                            <li><a href="item/item18.php"><img src="images/a1.jpg" class="img-responsive" alt=""/></a></li>
                                                            <li><a href="item/item16.php"><img src="images/a2.jpg" class="img-responsive" alt=""/></a></li>
                                                            <li><a href="item/item17.php"><img src="images/a3.jpg" class="img-responsive" alt=""/></a></li>
                                                            <li><a href="item/item21.php"><img src="images/a4.jpg" class="img-responsive" alt=""/></a></li>
                                                            <li><a href="item/item22.php"><img src="images/a5.jpg" class="img-responsive" alt=""/></a></li>
                                                            <li><a href="Art.php"><img src="images/t6.jpg" class="img-responsive" alt=""/></a></li>
                                                        </ul>
                                                    </div>

                                                </div>
                                            </div>
                                            <!-- Nav tabs -->

                                        </div>				
                                    </li>
                                    <li><a href="products.php">Products</a></li>
									<li><a href="account.php">Sign In</a></li>
                                    <li class="last"><a href="contact.php">Contact</a></li>
                            
                            
                            
						</ul>
					 </div><!-- /.navbar-collapse -->
				  
				</nav>
			</div>
			
				
		<div class="clearfix"> </div>
			<!---pop-up-box---->   
					<link href="css/popuo-box.css" rel="stylesheet" type="text/css" media="all"/>
					<script src="js/jquery.magnific-popup.js" type="text/javascript"></script>
					<!---//pop-up-box---->
				<div id="small-dialog" class="mfp-hide">
				<div class="search-top">
						<div class="login">
							<form action="search.php" method="post">
								<input type="submit" value="">
								<input type="text" name="search" value="Type something..." onfocus="this.value = '';" onblur="if (this.value == '') {this.value = '';}">		
							
							</form>
						</div>
						<p>	Shopping</p>
					</div>				
				</div>
				 <script>
						$(document).ready(function() {
						$('.popup-with-zoom-anim').magnificPopup({
							type: 'inline',
							fixedContentPos: false,
							fixedBgPos: true,
							overflowY: 'auto',
							closeBtnInside: true,
							preloader: false,
							midClick: true,
							removalDelay: 300,
							mainClass: 'my-mfp-zoom-in'
						});
																						
						});
				</script>			
	<!---->		
		</div>
	</div>
</div>
<!--banner-->
<div class="banner">
	<div class="matter-banner">
	 	<div class="slider">
	    	<div class="callbacks_container">
	      		<ul class="rslides" id="slider">
	        		<li>
	          			<img src="images/1.jpg" alt="">
						<div class="tes animated wow fadeInUp animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInUp;">
							<h2>THE WALL ART</h2>
							<h3>A web site to buy, sell artistry and hire artists</h3>
							<h4></h4>
							<p></p>
						</div>
	       			 </li>
			 		 <li>
	          			<img src="images/3.jpg" alt=""> 
						<div class="tes animated wow fadeInUp animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInUp;">
							<h2>THE WALL ART</h2>
							<h3>A web site to buy, sell artistry and hire artists</h3>
							<h4></h4>
							<p></p>
						</div>					
	       			 </li>
					 <li>
	          			<img src="images/2.jpg" alt="">
						<div class="tes animated wow fadeInUp animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInUp;">
							<h2>THE WALL ART</h2>
							<h3>A web site to buy, sell artistry and hire artists</h3>
							<h4></h4>
							<p></p>
						</div>
	        		</li>	
	      		</ul>
	 	 	</div>
		</div>
	</div>
	<div class="clearfix"> </div>
</div>
<!--//banner-->
<!--content-->
<div class="content">
	<div class="container">
		<div class="content-top">
			<div class="content-top1">
				<div class="col-md-3 col-md2 animated wow fadeInLeft" data-wow-delay=".5s">
					<div class="col-md1 simpleCart_shelfItem">
						<a href="single.html">
							<a href="item/item1.php"><img class="img-responsive" src="images/pi5.png" alt="" href="item1.php" /></a>
						</a>
						<h3>&nbsp;</h3>
						<div class="price">
						  <h5 class="item_price">Rs 4500</h5>
						  <div class="clearfix"> </div>
						</div>
					</div>
				</div>	
				<div class="col-md-6 animated wow fadeInDown animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInDown;">
					<div class="col-md3">
						<div class="up-t">
							<h3>Flat 50% Offer</h3>
						</div>
					</div>
				</div>
			<div class="col-md-3 col-md2 animated wow fadeInRight" data-wow-delay=".5s">
					<div class="col-md1 simpleCart_shelfItem">
						<a href="single.html">
							<a href="item/item2.php"><img class="img-responsive" src="images/pi4.png" alt="" /></a>
						</a>
						<h3>&nbsp;</h3>
						<div class="price">
								<h5 class="item_price">Rs 3000</h5>
								
								<div class="clearfix"> </div>
						</div>
					</div>
				</div>	
			<div class="clearfix"> </div>
			</div>	
			<div class="content-top1">
				<div class="col-md-3 col-md2 animated wow fadeInLeft" data-wow-delay=".5s">
					<div class="col-md1 simpleCart_shelfItem">
						<a href="single.html">
							<a href="item/item3.php">	<img class="img-responsive" src="images/pi3.png" alt="" /></a>
						</a>
						<h3>&nbsp;</h3>
						<div class="price">
								<h5 class="item_price">Rs 3000</h5>
								
								<div class="clearfix"> </div>
						</div>
					</div>
				</div>	
			<div class="col-md-3 col-md2 animated wow fadeInLeft" data-wow-delay=".5s">
					<div class="col-md1 simpleCart_shelfItem">
						<a href="single.html">
							<a href="item/item4.php"><img class="img-responsive" src="images/pi2.png" alt="" /></a>
						</a>
						<h3>&nbsp;</h3>
						<div class="price">
								<h5 class="item_price">Rs 3000</h5>
								<div class="clearfix"> </div>
						</div>
					</div>
				</div>	
			<div class="col-md-3 col-md2 animated wow fadeInRight" data-wow-delay=".5s">
					<div class="col-md1 simpleCart_shelfItem">
						<a href="single.html">
							<a href="item/item5.php"><img class="img-responsive" src="images/pi6.png" alt="" /></a>
						</a>
						<h3>&nbsp;</h3>
						<div class="price">
								<h5 class="item_price">Rs 3500</h5>
								
								<div class="clearfix"> </div>
						</div>
					</div>
				</div>	
			<div class="col-md-3 col-md2 cmn animated wow fadeInRight" data-wow-delay=".5s">
					<div class="col-md1 simpleCart_shelfItem">
						<a href="single.html">
							<a href="item/item6.php"><img class="img-responsive" src="images/pi8.png" alt="" /></a>
                            </a>
						
						<h3>&nbsp;</h3>
						<div class="price">
								<h5 class="item_price">Rs 3500</h5>
								
								<div class="clearfix"> </div>
						</div>
					</div>
				</div>	
			<div class="clearfix"> </div>
			</div>			
		</div>
	</div>
</div>
<div class="clearfix"> </div>
			</div>	
			
			<div class="clearfix"> </div>
			</div>			
		</div>
	</div>
</div>
<!--//content-->
	<div class="con-tp">
    <div style="margin-left:100px;">
        <h7 sylyle="color:pink;marginright:30px;">OFFERS</h7>
        </div>
		<div class="container">
			<div class="col-md-4 con-tp-lft animated wow fadeInLeft" data-wow-delay=".5s">
				
					<div class="content-grid-effect slow-zoom vertical">
						<div class="img-box"><img src="images/6.jpg" alt="image" class="img-responsive zoom-img"></div>
						<div class="info-box">
							<div class="info-content simpleCart_shelfItem">
										<h4>30%offer</h4>
							</div>
						</div>
					</div>
				</a>
			</div>
			<div class="col-md-4 con-tp-lft animated wow fadeInDown animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInDown;">
							
					<div class="content-grid-effect slow-zoom vertical">
						<div class="img-box"><img src="images/10.jpg" alt="image" class="img-responsive zoom-img"></div>
							<div class="info-box">
								<div class="info-content simpleCart_shelfItem">
										<h4>45%offer</h4>	
								</div>
							</div>
					</div>
				</a>
			</div>
			<div class="col-md-4 con-tp-lft animated wow fadeInRight" data-wow-delay=".5s">
				
					<div class="content-grid-effect slow-zoom vertical">
						<div class="img-box"><img src="images/9.jpg" alt="image" class="img-responsive zoom-img"></div>
							<div class="info-box">
								<div class="info-content simpleCart_shelfItem">
										<h4>50%offer</h4>	
								</div>
							</div>
					</div>
				</a>
			</div>
			<div class="clearfix"></div>
		<div class="col-md-4 con-tp-lft animated wow fadeInLeft" data-wow-delay=".5s">
			
				<div class="content-grid-effect slow-zoom vertical">
					<div class="img-box"><img src="images/12.jpg" alt="image" class="img-responsive zoom-img"></div>
						<div class="info-box">
							<div class="info-content simpleCart_shelfItem">
									<h4>25%offer</h4>	
							</div>
						</div>
				</div>
			</a>
		</div>
		<div class="col-md-4 con-tp-lft animated wow fadeInUp animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInUp;">
			
				<div class="content-grid-effect slow-zoom vertical">
					<div class="img-box"><img src="images/13.jpg" alt="image" class="img-responsive zoom-img"></div>
						<div class="info-box">
							<div class="info-content simpleCart_shelfItem">
									<h4>50%offer</h4>	
							</div>
						</div>
				</div>
			</a>
		</div>
		<div class="col-md-4 con-tp-lft animated wow fadeInRight" data-wow-delay=".5s">
			
				<div class="content-grid-effect slow-zoom vertical">
					<div class="img-box"><img src="images/14.jpg" alt="image" class="img-responsive zoom-img"></div>
						<div class="info-box">
							<div class="info-content simpleCart_shelfItem">
									<h4>35%offer</h4>	
							</div>
						</div>
				</div>
			</a>
		</div>
		<div class="clearfix"></div>
		</div>
	</div>
	<div class="c-btm">
		<div class="content-top1">
			<div class="container">
				<div class="col-md-3 col-md2 animated wow fadeInLeft" data-wow-delay=".5s">
					<div class="col-md1 simpleCart_shelfItem">
						<a href="single.html">
							<a href="item/item7.php"><img class="img-responsive" src="images/pi9.png" alt="" /></a>
						</a>
						<h3>&nbsp;</h3>
						<div class="price">
								<h5 class="item_price">Rs 3000</h5>
								
								<div class="clearfix"> </div>
						</div>
						
					</div>
				</div>	
			<div class="col-md-3 col-md2 animated wow fadeInLeft" data-wow-delay=".5s">
					<div class="col-md1 simpleCart_shelfItem">
						<a href="single.html">
							<a href="item/item8.php"><img class="img-responsive" src="images/pi10.png" alt="" /></a>
						</a>
						<h3>&nbsp;</h3>
						<div class="price">
								<h5 class="item_price">Rs 4500</h5>
								
								<div class="clearfix"> </div>
						</div>
						
					</div>
				</div>	
			<div class="col-md-3 col-md2 animated wow fadeInRight" data-wow-delay=".5s">
					<div class="col-md1 simpleCart_shelfItem">
						<a href="single.html">
							<a href="item/item9.php"><img class="img-responsive" src="images/pi11.png" alt="" /></a>
						</a>
						<h3>&nbsp;</h3>
						<div class="price">
								<h5 class="item_price">Rs 3500</h5>
								
								<div class="clearfix"> </div>
						</div>
						
					</div>
				</div>	
			<div class="col-md-3 col-md2 animated wow fadeInRight" data-wow-delay=".5s">
					<div class="col-md1 simpleCart_shelfItem">
						<a href="single.html">
							<a href="item/item10.php"><img class="img-responsive" src="images/pi12.png" alt="" /></a>
						</a>
						<h3>&nbsp;</h3>
						<div class="price">
								<h5 class="item_price">Rs 4000</h5>
								
								<div class="clearfix"> </div>
						</div>
						
					</div>
				</div>	
			<div class="clearfix"> </div>
			</div>	
		</div>
	</div>
<!--footer-->
<div class="footer">
	<div class="container">
		<div class="footer-top">
			<div class="col-md-6 top-footer animated wow fadeInLeft" data-wow-delay=".5s">
				<h3>Follow Us On</h3>
				<div class="social-icons">
					<ul class="social">
						<li><a href="#"><i></i></a> </li>
						<li><a href="#"><i class="facebook"></i></a></li>	
						<li><a href="#"><i class="google"></i> </a></li>
						<li><a href="#"><i class="linked"></i> </a></li>						
					</ul>
						<div class="clearfix"></div>
				 </div>
			</div>
			<div class="col-md-6 top-footer1 animated wow fadeInRight" data-wow-delay=".5s">
				<h3>Newsletter</h3>
					<form action="#" method="post">
						<input type="text" name="email" value="" onfocus="this.value='';" onblur="if (this.value == '') {this.value ='';}">
						<input type="submit" value="SUBSCRIBE">
					</form>
			</div>
			<div class="clearfix"> </div>	
		</div>	
	</div>
		<div class="footer-bottom">
		<div class="container">
				<div class="col-md-3 footer-bottom-cate animated wow fadeInLeft" data-wow-delay=".5s">
					<h6>Categories</h6>
					<ul>
						<li><a href="products.php">All Categories</a></li>
						<li><a href="paintings.php">Paintings</a></li>
						<li><a href="photographs.php">Photographs</a></li>
						<li><a href="Art.php">Art & Crafts</a></li>
						
						
					</ul>
				</div>
				<div class="col-md-3 footer-bottom-cate animated wow fadeInLeft" data-wow-delay=".5s">
					<h6>Feature Works</h6>
					<ul>
						<li><a href="item/item1.php">WORK 1</a></li>
						<li><a href="item/item2.php">WORK 2</a></li>
						<li><a href="item/item3.php">WORK 3</a></li>
						<li><a href="item/item4.php">WORK 4</a></li>
						<li><a href="item/item5.php">WORK 5</a></li>
					</ul>
				</div>
				<div class="col-md-3 footer-bottom-cate animated wow fadeInRight" data-wow-delay=".5s">
					<h6>Top Works</h6>
					<ul>
						<li><a href="item/item7.php">Top Work 1</a></li>
						<li><a href="item/item8.php">Top Work 2</a></li>
						<li><a href="item/item9.php">Top Work 3</a></li>
						<li><a href="item/item10.php">Top Work 4</a></li>
						<li><a href="item/item11.php">Top Work 5</a></li>
						
						
					</ul>
				</div>
				<div class="col-md-3 footer-bottom-cate cate-bottom animated wow fadeInRight" data-wow-delay=".5s">
					<h6>Contact Us</h6>
					<ul>
						<li><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i>Address : Wayamba University</li>
						<li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i>Email : <a href="mailto:info@example.com">info@example.com</a></li>
						<li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>Phone : +94 71 5 567 567</li>
					</ul>
				</div>
				<div class="clearfix"> </div>
				<p class="footer-class animated wow fadeInUp animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInUp;"> © 2017 THE WALL ART  - The Art Gallery . All Rights Reserved | Design by WUSL </p>
			</div>
	</div>
</div>
        <!--footer-->
         </body>
        </html>